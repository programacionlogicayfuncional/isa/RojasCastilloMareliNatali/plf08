(ns plf08.core-test
  (:require [clojure.test :refer [deftest is testing]]
            [plf08.core :refer [encriptar-tcs desencriptar-tcs]]))

(deftest encriptar-tcs-test
  (testing "Bloque pruebas 1"
    (is (= "hksutsileybe" (encriptar-tcs "cat" "theskyisblue")))
    (is (= "hbtssukeeliy" (encriptar-tcs "CanciOn" "theskyisblue")))
    (is (= "hbtssukeeliy" (encriptar-tcs "cAnCion" "theskyisblue"))))
  
  )

(deftest desencriptar-tcs-test
  (testing "Bloque pruebas 1"
    (is (= "theskyisblue" (desencriptar-tcs "cat" "hksutsileybe")))
    (is (= "theskyisblue" (desencriptar-tcs "cAT" "hksutsileybe")))
    (is (= "theskyisblue" (desencriptar-tcs "CaT" "hksutsileybe")))
  ))