# Descripción del proyecto.
Proyecto para subir cifrado de Polibio o Transposición columnar simple
# Forma de ejecutar.
pasar 4 o 5 parametros después de lein run, esto depende del cifrado

# Opciones para su ejecución.
Ejecución desde consola
# Ejemplos de uso o ejecución.
>lein run "cifradoPolibio" "cifrado" "kafka.txt" "archivo1.txt"

>lein run "cifradoPolibio" "descifrado" "kafka.txt" "archivo1.txt"

>lein run "cifradotcs" "encriptado" "cancion" "kafka.txt" "archivo1.txt"

>lein run "cifradotcs" "encriptado" "CaNcion" "kafka.txt" "archivo1.txt"
# Errores, limitantes, situaciones no consideradas o particularidades de tu solución.