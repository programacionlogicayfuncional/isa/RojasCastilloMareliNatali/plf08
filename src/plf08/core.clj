(ns plf08.core
  (:gen-class))

;=>>>>>>>>Cifrad de Polibio

(defn sutitución-alf
  [c]
  (let [a ["♜♜" "♜♞" "♜♝" "♜♛" "♜♚" "♜♖" "♜♘" "♜♗" "♜♕" "♜♔"
           "♞♜" "♞♞" "♞♝" "♞♛" "♞♚" "♞♖" "♞♘" "♞♗" "♞♕" "♞♔"
           "♝♜" "♝♞" "♝♝" "♝♛" "♝♚" "♝♖" "♝♘" "♝♗" "♝♕" "♝♔"
           "♛♜" "♛♞" "♛♝" "♛♛" "♛♚" "♛♖" "♛♘" "♛♗" "♛♕" "♛♔"
           "♚♜" "♚♞" "♚♝" "♚♛" "♚♚" "♚♖" "♚♘" "♚♗" "♚♕" "♚♔"
           "♖♜" "♖♞" "♖♝" "♖♛" "♖♚" "♖♖" "♖♘" "♖♗" "♖♕" "♖♔"
           "♘♜" "♘♞" "♘♝" "♘♛" "♘♚" "♘♖" "♘♘" "♘♗" "♘♕" "♘♔"
           "♗♜" "♗♞" "♗♝" "♗♛" "♗♚"]
        d [\a \á \b \c \d \e \é \f \g \h \i \í \j \k \l \m \n \ñ \o
           \ó \p \q \r \s \t \u \ú \ü \v \w \x \y \z \0 \1 \2 \3 \4
           \! \" \# \$ \% \& \' \( \) \* \+ \, \- \. \/ \: \; \< \=
           \> \? \@ \[ \\ \] \^ \_ \` \{ \| \} \~ \5 \6 \7 \8 \9]]
    (if (= c "cifrado") (zipmap d a) (zipmap a d))))


;=>                  CIFRADO
(defn cifrar-p
  [a]
  (let [b (apply str (replace (sutitución-alf "cifrado") a))]
    b))


;=>                 DESCIFRADO
(defn descifrar-p
  [s]
  (let [f (vector (take-nth 2 (map str s (drop 1 s))))]
    (apply str (replace (sutitución-alf "decifrado")
                        f))))



;app
(defn ejecucionP
  [a b c]
  (let [y  (slurp b)
        z (cond (= a "cifrado") (cifrar-p y) 
                (= a "decifrado")(descifrar-p y))]
    (spit c z)))





;=>>>>>>>>Cifrad Transposición columnar simple



(defn alfabeto
  [s]
  (vec (map
        (zipmap
         [\a \A \á \Á \b \B \c \C \d \D \e \E \é \É \f \F \g \G \h \H \i \I \í \Í
          \j \J \k \K \l \L \m \M \n \N \ñ \Ñ \o \O \ó \Ó \p \P \q \Q \r \R \s \S
          \t \T \u \U \ú \Ú \ü \Ü \v \V \w \W \x \X \y \Y \z \Z \0 \1 \2 \3 \4
          \! \" \# \$ \% \& \' \( \) \* \+ \, \- \. \/ \: \; \< \=
          \> \? \@ \[ \\ \] \^ \_ \` \{ \| \} \~ \5 \6 \7 \8 \9]
         [1 1 2 2 3 3 4 4 5 5 6 6 7 7 8 8 9 9 10 10 11 11 12 12 13 13 14 14
          15 15 16 16 17 17 18 18 19 19 20 20 21 21 22 22 23 23 24 24 25 25
          26 26 27 27 28 28 29 29 30 30 31 31 32 32 33 33 34 35 36 37 38 39
          40 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60 61
          62 63 64 65 66 67 68 69 70 71 72 73 74 75])
        s)))

(defn encriptar-tcs
  [a b]
  (let [z (map-indexed (fn [idx itm] {(mod idx (count a))  itm}) b)
        y (mapv vec (vals (group-by keys z)))
        w (let [ys y]
            (for [x ys]
              (vec (map vec (map vals x)))))
        v (let [ys w]
            (for [x ys]
              (vec (flatten
                    (map conj x)))))]
    (apply str (flatten (map second (sort-by first (map vector (alfabeto a) (vec v))))))))





(defn desencriptar-tcs
  [y z]
  (let [a (partition-all (/ (count z) (count y)) z)
        b (filterv char? (flatten (map flatten (sort-by first (map vector
                                                                   (sort-by second (map-indexed vector (alfabeto y)))
                                                                   (map vector (alfabeto (sort y)) (vec a)))))))
        c (vec (map vec (partition-all (/ (count z) (count y)) b)))
        d (let [ys (vec c)]
            (for [x ys]
              (map-indexed (fn [idx itm] {idx  itm}) x)))
        e (mapv vec (vals (group-by keys (flatten d))))
        f (let [ys e]
            (for [x ys]
              (vec (map vec x))))
        g (let [ys (vec f)]
            (for [x ys]
              (vec (flatten (flatten x)))))
        h (sort-by first g)
        i (let [ys (vec h)]
            (for [x ys]
              (filter char? x)))]
    (apply str (flatten i))))


(defn ejecucionTcs
  [a b c d]
  (let [
        y  (slurp c)
        z (cond 
            (= a "encriptado") (encriptar-tcs b y) 
            (= a "desencriptado")(desencriptar-tcs b y))]
    (spit d z)))



;=> Función -main

(defn -main
  (
   [a b c d]
   (= a "cifradoPolibio")
   (if (empty? d)
    (println "Faltan argumentos");; ¿A qué se debe el error? ¿Alguna forma de indicarle al usuario la razón?
    (ejecucionP b c d)
    ))
   ([a b c d e]
     (= a "cifradotcs")
     (if (empty? d)
     (println "Faltan argumentos");; ¿A qué se debe el error? ¿Alguna forma de indicarle al usuario la razón?
     (ejecucionTcs b c d e)))
  )










